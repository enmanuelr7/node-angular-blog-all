# HN Feed
click [here](https://hnfeed-b0767.web.app/ "here") for a live demo. It could take some seconds to fetch articles since heroku server turns sleep mode after 30 minutes of inactivity.

When server code runs for the first time it fetches a batch of articles and stores it in the database in a collection called blogs. The process repeats once an hour ignoring duplicate articles.

Angular version `10.1.6`
Node version `12.19.0`

## Steps for running project locally
You will need a mongodb connection url (local or cloud)
- run `git clone --recurse-submodules https://gitlab.com/enmanuelr7/node-angular-blog-all.git` for cloning repository including submodules.
- run `npm install` on each root project folder (client and server)
- add `.env` file on server project root folder and insert `DATABASE_URL=<your database connection url>` where `<your database connection url>` is the connection string of a mongodb database
- run `npm run start` on server project root folder. It will run the server project on local port 3000
- run `ng serve -o` on client project root folder. It will run the client project locally and open a browser window

